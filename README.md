# Agent Cluster Image Scanning Demo

This repository demonstrates an example of [Cluster Image Scanning with the GitLab Agent](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/#cluster-image-scanning-with-the-gitlab-kubernetes-agent).

## Prerequisites

- Docker
- k3d
- kubectl
- helm
- k9s (optional)

## How To

1. Fork this repo
1. Create a k3d cluster

   ```shell
   k3d cluster create -p "8081:80@loadbalancer" cis-demo
   ```

1. [Install the GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html).
   Use this repository as the configuration repository. Make sure the agent is named `cis-demo`.
1. Deploy hello world app from this repository

   ```shell
   kubectl apply -f deploy/hello-world-app.yml
   ```

1. Agent will create vulnerabilities in GitLab
1. View vulnerabilities on the security dashboard
