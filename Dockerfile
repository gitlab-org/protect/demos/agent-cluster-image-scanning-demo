FROM golang:1.17

COPY . /build/

WORKDIR /build/

RUN go build -o /server .

ENTRYPOINT [ "/server" ]
